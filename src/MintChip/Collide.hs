module MintChip.Collide (isColliding) where

import MintChip.Rect

isColliding :: (Rect a, Rect b) => a -> b -> Bool
isColliding a b = abs xDiff < maxXDiff && abs yDiff < maxYDiff
  where maxXDiff = (width a `div` 2) + (width b `div` 2)
        maxYDiff = (height a `div` 2) + (height b `div` 2)
        xDiff = aCenterX - bCenterX
        yDiff = aCenterY - bCenterY
        aCenterX = x a + (width a `div` 2)
        bCenterX = x b + (width b `div` 2)
        aCenterY = y a + (height a `div` 2)
        bCenterY = y b + (height b `div` 2)

module MintChip.Entities.Player (updatePlayer) where

import qualified Data.Vector as V

import Control.Lens
import Control.Monad.State
import Linear (V2(..))

import MintChip.Entities.Ammo
import MintChip.Collide
import MintChip.Input
import MintChip.Rect
import MintChip.Renderer
import MintChip.Entities.Token
import MintChip.Types

updatePlayer :: MintChip ()
updatePlayer = 
  movePlayer
  >> handleAmmoFiring
  >> handleObstacles

movePlayer :: MintChip ()
movePlayer = do
  upPressed <- liftIO $ controlIsPressed ControlAction'Up
  downPressed <- liftIO $ controlIsPressed ControlAction'Down
  -- move up on keypress
  when upPressed $ player.playerPos._2 -= 1
  -- move down on keypress
  when downPressed $ player.playerPos._2 += 1
  -- move forward constantly
  player.playerPos._1 += 1

handleAmmoFiring :: MintChip ()
handleAmmoFiring = do
  player' <- gets (^.player)
  firePressed <- liftIO $ controlIsPressed ControlAction'FireLaser
  -- Update firing delay.
  -- Firing ammo adds to the delay, so you have to wait for the
  -- delay to go back down (so the ship's weapons re-charge) before
  -- firing another shot.
  when ((player'^.playerFireDelay) > 0) $ player.playerFireDelay -= 1
  -- Fire lasers if user presses the fire key.
  -- The player has the special advantage of two laser cannons that
  -- fire simultaneously to no extra cost on recharging.
  when (firePressed && (player'^.playerFireDelay) == 0) $ do
    let theAmmo = AmmoType'Laser Alignment'Good
    fireAmmo theAmmo (V2 (width player' + 5) (height player' `div` 2 - 3) + (player'^.playerPos))
    fireAmmo theAmmo (V2 (width player' + 5) (height player' `div` 2 + 3) + (player'^.playerPos))
    player.playerFireDelay += getFireDelay theAmmo

-- | Handle interaction with tokens, ammo, and tiles
handleObstacles :: MintChip ()
handleObstacles = do
  ammo <- gets (^.ammo)
  tokens <- gets (^.tokens)
  terrain <- gets (^.terrain)
  player' <- gets (^.player)
  -- check ammo, tokens, & tiles for collision
  V.mapM_ checkAmmo ammo
  V.mapM_ checkToken tokens
  V.mapM_ checkTile terrain
  -- check if player is dead
  when (player'^.playerHp <= 0) $ do
    player.playerIsDead .= True
    player.playerHp .= 0 -- don't let health be negative

-- | Respond to collisions with @Ammo@.
checkAmmo :: Ammo -> MintChip ()
checkAmmo ammo = gets (^.player) >>= \player ->
  when (player `isColliding` ammo) $ handleAmmo ammo
  
handleAmmo :: Ammo -> MintChip ()
handleAmmo ammo = player.playerHp -= ammoDamage (ammo^.ammoType)

-- | Respond to collisions with @Token@s.
checkToken :: Token -> MintChip ()
checkToken token = gets (^.player) >>= \player ->
  when (player `isColliding` token) $ handleTokenCollision token

handleTokenCollision :: Token -> MintChip ()
handleTokenCollision token = 
  case token^.tokenType of
    TokenType'Heart -> player.playerHp += 15
    TokenType'IceCream -> player.score += 5

-- | Respond to collisions with tiles.
-- Spoiler alert: running into solid matter in a high-speed
-- spaceship will always cause you to explode and die.
checkTile :: Tile -> MintChip ()
checkTile tile = gets (^.player) >>= \player' ->
  when (player' `isColliding` tile) $ player.playerHp .= 0

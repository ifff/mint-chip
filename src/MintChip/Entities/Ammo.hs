module MintChip.Entities.Ammo
  ( ammoDamage
  , ammoVelocity
  , fireAmmo
  , getFireDelay
  , updateAmmo ) where

import qualified Data.Vector as V

import Control.Lens
import Control.Monad
import Control.Monad.Reader
import Control.Monad.State
import Linear (V2(..))

import MintChip.Collide
import MintChip.Rect
import MintChip.Renderer
import MintChip.Types

-- | Add a new @Ammo@ thing to the list of @Ammo@.
fireAmmo :: AmmoType -> V2 Int -> MintChip ()
fireAmmo atype pos = ammo %= V.cons newAmmo
  where newAmmo = Ammo pos atype False
  
updateAmmo :: Ammo -> MintChip Ammo
updateAmmo ammo = do
  ww <- asks (^.windowWidth)
  player <- gets (^.player)
  terrain <- gets (^.terrain)
  
  let vel = ammoVelocity $ ammo^.ammoType
      newPos = _1 +~ vel $ ammo^.ammoPos
      newIsDead = ammo `isColliding` player || any (isColliding ammo) terrain
  
  return ammo { _ammoPos = newPos, _ammoIsDead = newIsDead }

-- | Get the delay experienced after firing ammo.
getFireDelay :: AmmoType -> Int
getFireDelay (AmmoType'Laser _) = 75
getFireDelay _ = undefined

-- | Get the damage the ammo does.
ammoDamage :: AmmoType -> Int
ammoDamage (AmmoType'Laser _) = 5
ammoDamage _ = undefined

-- | Get the velocity of the ammo. Ammo velocity is like speed, but with a direction.
ammoVelocity :: AmmoType -> Int
ammoVelocity ammo@(AmmoType'Laser alignment) = alignment `directing` ammoSpeed ammo
ammoVelocity _ = undefined

-- | Get the speed of the ammo. Ammo speed is the absolute value of ammo velocity.
ammoSpeed :: AmmoType -> Int
ammoSpeed (AmmoType'Laser _) = 5
ammoSpeed _ = undefined
  
-- | Direct an ammo's velocity either forwards or backwards
-- based on its alignment (Good => moves right; Bad => moves left)
directing :: (Num a, Ord a) => Alignment -> a -> a
directing Alignment'Bad n
  | n < 0 = n
  | otherwise = -n
directing Alignment'Good n
  | n > 0 = n
  | otherwise = -n


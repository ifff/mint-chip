module MintChip.Entities.Enemy (updateEnemy) where

import Control.Lens
import Control.Monad
import Control.Monad.Reader
import Control.Monad.State
import Linear (V2(..))

import MintChip.Entities.Ammo
import MintChip.Collide
import MintChip.Rect
import MintChip.Renderer
import MintChip.Types

updateEnemy :: Enemy -> MintChip Enemy
updateEnemy enemy = do
  ammo <- gets (^.ammo)
  player <- gets (^.player)
  w <- asks (^.windowWidth)
  -- update health & fire delay
  let newFireDelay = if (enemy^.enemyFireDelay) == 0 then
                       (enemy^.enemyFireDelay) + getFireDelay (enemy^.enemyAmmoType)
                     else enemy^.enemyFireDelay
      totalDamage = sum $ checkAmmoHits enemy <$> ammo
      newHp = if totalDamage < (enemy^.enemyHp) then
                (enemy^.enemyHp) - totalDamage
              else 0
  -- fire ammo if applicable
  when ((enemy^.enemyFireDelay) == 0 && x enemy < (x player + w)) $
    fireAmmo (enemy^.enemyAmmoType) ((V2 (-10) (height enemy `div` 2)) + (enemy^.enemyPos))
  -- return new state of enemy
  return enemy { _enemyHp = newHp
               , _enemyIsDead = newHp == 0
               , _enemyFireDelay = newFireDelay - 1 }

-- | If ammo is colliding the enemy, return the amount of
-- damage the ammo inflicted; otherwise, return 0.
checkAmmoHits :: Enemy -> Ammo -> Int
checkAmmoHits enemy ammo
  | True <- enemy `isColliding` ammo,
    (AmmoType'Laser Alignment'Good) <- ammo^.ammoType = ammoDamage (ammo^.ammoType)
  | otherwise = 0

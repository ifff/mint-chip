module MintChip.Loader.SpriteLoader (loadSprites) where

import qualified SDL
import qualified SDL.Sprite as Sprite

import Control.Lens
import Linear (V2(..), V4(..))

import MintChip.Types

-- | Loads the sprites.
loadSprites :: SDL.Renderer -> IO Sprites
loadSprites ren = do
  playerSprite <- Sprite.load ren "assets/ship.png" (V2 32 32)
  explosionSprite <- Sprite.load ren "assets/explosion.png" (V2 32 32)  
  return $ Sprites
    { _playerSprite = playerSprite
    , _explosionSprite = explosionSprite}

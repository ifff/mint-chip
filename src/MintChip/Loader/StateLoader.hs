module MintChip.Loader.StateLoader (loadGameState) where

import qualified Data.Vector as V

import Control.Lens
import Linear.V2 (V2(..))

import MintChip.Loader.SpriteLoader
import MintChip.Loader.TilemapLoader
import MintChip.Types

loadGameState :: GameConfig -> IO GameState
loadGameState cfg = do
  sprites <- loadSprites (cfg^.ren)
  (terrain, enemies, tokens) <- loadTilemapData "assets/test-map.json"
  let initialPlayer = Player
        { _playerHp = 100
        , _score = 0
        , _playerPos = V2 0 ((cfg^.windowHeight) `div` 2)
        , _playerIsDead = False
        , _playerFireDelay = 0 }
      initialState = GameState
        { _player = initialPlayer
        , _tokens = tokens
        , _enemies = enemies
        , _ammo = V.empty
        , _terrain = terrain
        , _mode = GameMode'Menu
        , _sprites = sprites }
  return initialState

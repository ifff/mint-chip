{-# LANGUAGE OverloadedStrings #-}

module MintChip.Loader.AssetLoader (loadAssets) where

import qualified SDL
import qualified SDL.Font as Font
import qualified SDL.Image as Image

import Control.Lens
import Data.Text (Text, pack)
import Linear (V2(..), V4(..))

import MintChip.Types

-- | Loads the (static) assets (images, audio, and whatever else) for the game to use.
loadAssets :: SDL.Renderer -> IO Assets
loadAssets ren = do
  -- load fonts at different sizes
  largeFont <- loadFont 20
  mediumFont <- loadFont 15
  smallFont <- loadFont 10
  -- make some Text Textures
  menuTitle <- makeText ren largeFont "MINT CHIP -- Press X to Start"
  highScoresTitle <- makeText ren mediumFont "High Scores: "
  healthTitle <- makeText ren mediumFont "Health: "
  scoreTitle <- makeText ren mediumFont "Score: "
  deathTitle <- makeText ren largeFont "You died! Press M to acknowledge."
  winTitle <- makeText ren largeFont "You win!!!"
  digits <- mapM (makeText ren mediumFont . pack . show) [0..9]  
  -- make some static images
  heartImg <- Image.loadTexture ren "assets/heart.png"
  iceCreamImg <- Image.loadTexture ren "assets/ice_cream.png"
  goodLaserImg <- Image.loadTexture ren "assets/good_laser.png"
  badLaserImg <- Image.loadTexture ren "assets/bad_laser.png"
  turretImg <- Image.loadTexture ren "assets/turret.png"
  blockImg <- Image.loadTexture ren "assets/block.png"
  stalagImg <- Image.loadTexture ren "assets/stalagmites.png"
  stalacImg <- Image.loadTexture ren "assets/stalactites.png"
  -- get rid of the fonts
  Font.free largeFont
  Font.free mediumFont
  Font.free smallFont
  -- return the assets
  return $ Assets
    { _menuTitle = menuTitle
    , _highScoresTitle = highScoresTitle
    , _healthTitle = healthTitle
    , _scoreTitle = scoreTitle
    , _deathTitle = deathTitle
    , _winTitle = winTitle
    , _heartImage = heartImg
    , _iceCreamImage = iceCreamImg
    , _goodLaserImage = goodLaserImg
    , _badLaserImage = badLaserImg
    , _turretImage = turretImg
    , _blockImage = blockImg
    , _stalagmitesImage = stalagImg
    , _stalactitesImage = stalacImg
    , _digits = digits }

-- | Loads the IBM VGA8 font at the specified size.
loadFont :: Font.PointSize -> IO Font.Font
loadFont = Font.load "assets/PxPlus_IBM_VGA8.ttf"

-- | Get text as an SDL Texture.
makeText :: SDL.Renderer -> Font.Font -> Text -> IO SDL.Texture
makeText ren font str = do
  surf <- Font.solid font (V4 255 255 255 255) str
  tex <- SDL.createTextureFromSurface ren surf
  SDL.freeSurface surf
  return tex

module MintChip.Loader.Loader
  (loadAssets, loadGameState) where

import MintChip.Loader.AssetLoader
import MintChip.Loader.StateLoader

{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE TemplateHaskell #-}

module MintChip.Types where

import qualified Data.Vector as V
import qualified SDL
import qualified SDL.Font as Font
import qualified SDL.Sprite as Sprite

import Control.Lens
import Control.Monad.Reader
import Control.Monad.State
import Linear (V2(..))

-- | The different control actions the user can make
data ControlAction =
  ControlAction'Up
  | ControlAction'Down
  | ControlAction'FireLaser
  | ControlAction'Quit
  | ControlAction'Enter
  | ControlAction'GoToMenu
  deriving (Eq, Show)

-- | The mode or scene of the game.
data GameMode =
  GameMode'Menu
  | GameMode'Play
  deriving (Eq, Show)

-- | The different types of enemies in the game.
data EnemyType =
  EnemyType'LaserTurret
  deriving (Eq, Show)

-- | The different types of tokens found in the game.
data TokenType =
  TokenType'Heart -- ^ Gives player more health.
  | TokenType'IceCream -- ^ Ice creams provide a score boost and are the reason for this game's name.
  deriving (Eq, Show)

-- | Represents whether something hurts the player or hurts the enemies.
data Alignment =
  Alignment'Bad
  | Alignment'Good
  deriving (Eq, Show)

-- | The different types of ammo that can be fired.
data AmmoType =
  AmmoType'Laser Alignment
  deriving (Eq, Show)

-- | The different types of tiles on the game map.
data TileType =
  TileType'Block -- ^ Normal rocks or whatever. Just don't run into them.
  | TileType'Stalagmites -- ^ Don't hit these!
  | TileType'Stalactites -- ^ Don't hit these, either
  deriving (Eq, Show)

-- | Tokens can be found floating around in the game, and are
-- collected by flying into them.
data Token = Token
  { _tokenPos :: V2 Int
  , _tokenType :: TokenType
  , _isCollected :: Bool
  } deriving (Show)

-- | All the data for a piece of ammunition.
data Ammo = Ammo
  { _ammoPos :: V2 Int
  , _ammoType :: AmmoType
  , _ammoIsDead :: Bool
  } deriving (Show)

-- | An enemy. These have various ammunition, capabilities, and directions.
data Enemy = Enemy
  { _enemyPos :: V2 Int
  , _enemyType :: EnemyType
  , _enemyHp :: Int
  , _enemyAmmoType :: AmmoType
  , _enemyIsDead :: Bool
  , _enemyFireDelay :: Int
  } deriving (Show)

data Tile = Tile
  { _tilePos :: V2 Int
  , _tileType :: TileType
  } deriving (Show)

data Player = Player
  { _playerHp :: Int
  , _score :: Int
  , _playerPos :: V2 Int
  , _playerIsDead :: Bool
  , _playerFireDelay :: Int
  } deriving (Show)

-- | A container for all the images/fonts and stuff.
-- Note that this holds _static_ assets, so no moving sprites.
data Assets = Assets
  { _menuTitle :: SDL.Texture
  , _highScoresTitle :: SDL.Texture
  , _healthTitle :: SDL.Texture
  , _scoreTitle :: SDL.Texture
  , _deathTitle :: SDL.Texture
  , _winTitle :: SDL.Texture
  , _heartImage :: SDL.Texture
  , _iceCreamImage :: SDL.Texture
  , _goodLaserImage :: SDL.Texture
  , _badLaserImage :: SDL.Texture
  , _turretImage :: SDL.Texture
  , _blockImage :: SDL.Texture
  , _stalagmitesImage :: SDL.Texture
  , _stalactitesImage :: SDL.Texture
  , _digits :: [SDL.Texture] }

-- | A container for all the sprites in the game.
-- This is meant to be put inside `GameState` since sprites must be updated.
data Sprites = Sprites
  { _playerSprite :: Sprite.Sprite
  , _explosionSprite :: Sprite.Sprite } 

-- | Configuration variables for the game.
data GameConfig = GameConfig
  { _ren :: SDL.Renderer
  , _win :: SDL.Window
  , _assets :: Assets
  , _windowWidth :: Int
  , _windowHeight :: Int
  , _finishLine :: Int } 

-- | The state of an MintChip game.
data GameState = GameState
  { _player :: Player
  , _tokens :: V.Vector Token
  , _enemies :: V.Vector Enemy
  , _ammo :: V.Vector Ammo
  , _terrain :: V.Vector Tile
  , _mode :: GameMode
  , _sprites :: Sprites }

newtype MintChip a = MintChip
  { runMintChip :: ReaderT GameConfig (StateT GameState IO) a }
  deriving (Applicative, Functor, Monad, MonadIO,
              MonadReader GameConfig, MonadState GameState)

-- Make pretty much everything a Lens so we don't waste braces while
-- digging through the guts of the game.
makeLenses ''Ammo
makeLenses ''Assets
makeLenses ''GameConfig
makeLenses ''Enemy
makeLenses ''GameState
makeLenses ''Player
makeLenses ''Sprites
makeLenses ''Tile
makeLenses ''Token

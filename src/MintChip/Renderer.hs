{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE GADTs #-}

module MintChip.Renderer
  ( Camera(..)
  , Renderable(..)
  , renderInt
  , renderSprite
  , renderTexture
  , updateSprites ) where

import qualified Data.FastDigits as Digits
import qualified SDL
import qualified SDL.Sprite as Sprite

import Control.Lens
import Control.Monad.Reader
import Control.Monad.State
import Data.Text (Text)
import Linear (V2(..))

import MintChip.Rect
import MintChip.Types

-- | A @Camera@ holds a @Rect@ representing the position for it to focus on.
data Camera a where
  Cam :: (Rect a) => a -> Camera a

-- | An @Image@ is something that SDL can draw (i.e. a Texture or Sprite).
class Image a where
  renderImage :: a -> V2 Int -> MintChip ()

instance Image SDL.Texture where
  renderImage = renderTexture

instance Image Sprite.Sprite where
  renderImage = renderSprite

-- | A @Renderable@ is a @Rect@ of type @a@ that can be represented and displayed
-- as an @Image@ of type @b@ with regards to a @Camera@ focus. 
class (Rect a, Image b) => Renderable a b | a -> b where
  getTexture :: a -> MintChip b
  
  -- | Get the location of @a@ relative to the camera
  displacing :: Camera c -> a -> V2 Int
  (Cam focus) `displacing` a = location a - location focus

  -- | Draw a @Renderable@ with regard to the camera only if it's within view of the screen.
  render :: Camera c -> a -> MintChip ()
  render cam a = do
    w <- asks (^.windowWidth)
    h <- asks (^.windowHeight)
    tex <- getTexture a
    let adjustedPos@(V2 x y) = cam `displacing` a
    -- check if @a@ is in view before rendering
    when (x >= -w && x <= w && y >= -h && y <= h) $
      renderImage tex adjustedPos

instance Renderable Ammo SDL.Texture where
  getTexture ammo = do
    goodLaserImg <- asks (^.assets.goodLaserImage)
    badLaserImg <- asks (^.assets.badLaserImage)    
    return $ case ammo^.ammoType of
               AmmoType'Laser Alignment'Good -> goodLaserImg
               AmmoType'Laser Alignment'Bad -> badLaserImg
               _ -> undefined

instance Renderable Enemy SDL.Texture where
  getTexture enemy = do
    turretImg <- asks (^.assets.turretImage)
    return $ case enemy^.enemyType of
               EnemyType'LaserTurret -> turretImg
               _ -> undefined

instance Renderable Player Sprite.Sprite where
  getTexture player
    | player^.playerIsDead = gets (^.sprites.explosionSprite)
    | otherwise = gets (^.sprites.playerSprite)

instance Renderable Tile SDL.Texture where
  getTexture tile = do
    blockImg <- asks (^.assets.blockImage)
    stalagImg <- asks (^.assets.stalagmitesImage)
    stalacImg <- asks (^.assets.stalactitesImage)
    return $ case tile^.tileType of
               TileType'Block -> blockImg
               TileType'Stalagmites -> stalagImg
               TileType'Stalactites -> stalacImg
  
instance Renderable Token SDL.Texture where
  getTexture token = do
    heartImg <- asks (^.assets.heartImage)
    iceCreamImg <- asks (^.assets.iceCreamImage)
    return $ case token^.tokenType of
               TokenType'Heart -> heartImg
               TokenType'IceCream -> iceCreamImg


-- | Render all the digits of an Int.
renderInt :: Int -> V2 Int -> MintChip ()
renderInt n (V2 x y) = asks (^.assets.digits) >>= \digits ->
  let digsWithCoords = zip digs [V2 x' y | x' <- [x, x+15..]]
      -- 0 is a special case, because @Digits.digits@ returns an empty list
      -- for its digits, and that obviously won't do
      digs = case n of
        0 -> [head digits]
        _ -> map (digits !!) . reverse $ Digits.digits 10 (fromIntegral n)
  in mapM_ (uncurry renderTexture) digsWithCoords

-- | Render a texture.
renderTexture :: SDL.Texture -> V2 Int -> MintChip ()
renderTexture tex (V2 x y) = do
  ren <- asks (^.ren)
  -- get width & height of texture
  info <- SDL.queryTexture tex
  let w = SDL.textureWidth info
      h = SDL.textureHeight info
      loc = V2 (fromIntegral x) (fromIntegral y)
      
  SDL.copy ren tex Nothing
    (Just $ SDL.Rectangle (SDL.P loc) (V2 w h))

-- | Render a sprite.
renderSprite :: Sprite.Sprite -> V2 Int -> MintChip ()
renderSprite sprite (V2 x y) = Sprite.render sprite loc
  where loc = V2 (fromIntegral x) (fromIntegral y)

-- | Update all the sprite animations.
updateSprites :: MintChip ()
updateSprites =
  sprites.playerSprite %= Sprite.animate
  >> sprites.explosionSprite %= Sprite.animate


# Mint Chip
Mint Chip is a side-scrolling game written in Haskell. To run it, execute:

```
$ stack exec mint-chip-exe 
```

(Note that you need stack installed to run it).

## Playing
The object of the game is to get the highest score possible. You can increase
your score by collecting the mint chip ice cream tokens, which are found in
hard-to-reach areas. Winning the game (by flying through the whole map without dying)
will give you a big score boost.

Hitting spikes or solid ground will kill you instantly, and hitting lasers
will decrease your health a bit. You can collect heart tokens to increase your
health again.

Press 'w' to move up, 's' to move down, and 'f' to fire your dual laser cannons.
You can use your lasers to kill the laser turrets.